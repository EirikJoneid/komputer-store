const elAccountBalance = document.getElementById("bankBalance");
const elWorkBalance = document.getElementById("workBalance");
const elLoanBalance = document.getElementById("loanBalance");
const work_btn = document.getElementById("work-btn");
const deposit_btn = document.getElementById("deposit-btn");
const loan_btn = document.getElementById("loan-btn");
const elLoanText = document.getElementById("loan-p");
const elRepay = document.getElementById("repay-div");
const computerSelect = document.getElementById("computerSelect");
const elDescription = document.getElementById("descSection");
const elPictureSection = document.getElementById("picSection");
const elbuySection = document.getElementById("buySection");
const elSpecsList = document.getElementById("specsList");

let accountBalance = 0;
let workBalance = 0;
let loanBalance = 0;
let loanCount = 0;
let hasBoughtComputer = false;
let computerData;

function handleWorkClick() {
  workBalance += 100;
  refreshBalanceDisplay();
}

// deposits pay into bank, and refreshes the HTML elements.
function handleDepositClick() {
  if (loanBalance === 0) {
    accountBalance += workBalance;
  } else {
    accountBalance += 0.9 * workBalance;
    loanBalance -= 0.1 * workBalance;
  }
  workBalance = 0;
  refreshBalanceDisplay();
}

// deposits loan into back account if loan criteria is met.
function handleLoanClick() {
  const requestedLoanAmount = parseInt(prompt("Please enter loan amount"));
  const loanStatus = checkLoanConstraints(requestedLoanAmount);

  if (loanStatus.isAproved) {
    accountBalance += requestedLoanAmount;
    loanBalance += requestedLoanAmount;
    loanCount++;
    refreshBalanceDisplay();
  } else {
    alert(loanStatus.message);
  }
}

// checks whether u can get a loan or not, and gives a reason.
function checkLoanConstraints(amount) {
  if (amount > accountBalance * 2) {
    return {
      isAproved: false,
      message: "U cannot loan more than double of your bank balance.",
    };
  }
  if (loanBalance != 0) {
    return {
      isAproved: false,
      message: "U cannot get a loan before paying back your previous loan.",
    };
  }
  if (!hasBoughtComputer && loanCount >= 1) {
    return {
      isAproved: false,
      message: "U cannot get another loan before buying a computer.",
    };
  }
  return {
    isAproved: true,
    message: "Congratulations! Ur loan has been aproved :)",
  };
}

// Repays loan from pay.
function handleRepayClick() {
  loanBalance -= workBalance;
  if (loanBalance < 0) {
    accountBalance -= loanBalance;
    loanBalance = 0;
  }
  workBalance = 0;
  refreshBalanceDisplay();
}

// refreshes HTML elements to display the correct balances and buttons.
function refreshBalanceDisplay() {
  elAccountBalance.innerText = accountBalance;
  elWorkBalance.innerText = workBalance;

  if (loanBalance != 0) {
    elLoanText.innerText = "Outstanding loan:";
    elLoanBalance.innerText = loanBalance;

    // when the user have a loan a repay-button is added.
    addRepayButton();
  } else {
    elLoanText.innerText = "";
    elLoanBalance.innerText = "";
    elRepay.innerHTML = "";
  }
}

// adds a repay-button to the DOM.
function addRepayButton() {
  const btn = document.createElement("button");
  btn.className = "btn btn-success";
  btn.innerText = "Repay loan";
  btn.onclick = () => {
    handleRepayClick();
  };
  btn.id = "paybackButton";
  elRepay.innerHTML = "";
  elRepay.appendChild(btn);
}

// fetches computerData from the computer-API and stores it in a global variable.
async function getComputerData() {
  const data = await fetch(
    "https://noroff-komputer-store-api.herokuapp.com/computers"
  );
  computerData = await data.json();
}

// fetches computerData from the API and populates the select element with options.
async function populateSelect() {
  await getComputerData();
  console.log(computerData);

  for (computer of computerData) {
    const option = document.createElement("option");
    option.innerText = computer.title;
    option.value = computer.id;
    computerSelect.appendChild(option);
  }
}

// is run when a computer is selected from the dropdown.
function handleComputerSelect() {
  const selectedComputer = getComputerByID(this.value);
  changePicture(selectedComputer);
  changeDescription(selectedComputer);
  changeBuySection(selectedComputer);
  changeLaptopsSection(selectedComputer);
}

// updates the description of the selectedComputer
function changeDescription(selectedComputer) {
  const title = document.createElement("h4");
  title.innerText = selectedComputer.title;

  const desc = document.createElement("p");
  desc.innerText = selectedComputer.description;

  elDescription.innerHTML = "";
  elDescription.appendChild(title);
  elDescription.appendChild(desc);
}

// updates the picture of the selectedComputer
function changePicture(selectedComputer) {
  const img = document.createElement("img");
  img.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
  img.width = "200";
  img.alt = "Could not load picture.";

  elPictureSection.innerHTML = "";
  elPictureSection.appendChild(img);
}

// updates the price of the selectedComputer and adds a buy button.
function changeBuySection(selectedComputer) {
  const price = document.createElement("h5");
  price.innerText = selectedComputer.price + " NOK";

  const buy_btn = document.createElement("button");
  buy_btn.className = "btn btn-primary";
  buy_btn.innerText = "Buy now";
  buy_btn.onclick = () => {
    handleBuyClick(selectedComputer);
  };

  elbuySection.innerHTML = "";
  elbuySection.appendChild(price);
  elbuySection.appendChild(buy_btn);
}

// shows a list of the selected computer's specs.
function changeLaptopsSection(selectedComputer) {
  elSpecsList.innerHTML = "";

  for (spec of selectedComputer.specs) {
    const li = document.createElement("li");
    li.innerText = spec;
    elSpecsList.appendChild(li);
  }
}

function getComputerByID(ID) {
  return computerData.find((computer) => computer.id == ID);
}

// Buys the selected computer if the user can afford it.
function handleBuyClick(selectedComputer) {
  if (accountBalance >= selectedComputer.price) {
    accountBalance -= selectedComputer.price;
    refreshBalanceDisplay();
    alert(`Congratulations! U bought a ${selectedComputer.title}`);
  } else {
    alert("U do not have enough money to buy this computer.");
  }
}

populateSelect();
work_btn.onclick = () => {
  handleWorkClick();
};
deposit_btn.onclick = () => {
  handleDepositClick();
};
loan_btn.onclick = () => {
  handleLoanClick();
};
computerSelect.onchange = handleComputerSelect;
